CREATE OR REPLACE FUNCTION XOR(first bigint, second bigint) RETURNS bigint AS $$
BEGIN
    RETURN (first::bit(64) # second::bit(64))::bigint;
END; $$
    LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION bit_count(value bigint) RETURNS numeric AS $$
BEGIN
    RETURN SUM((value >> bit) & 1) FROM generate_series(0, 63) bit;
END;$$
    LANGUAGE PLPGSQL;
