CREATE OR REPLACE FUNCTION CMP(first bigint, second bigint) RETURNS numeric AS $$
BEGIN
    RETURN 1 - bit_count(XOR(first, second))/64;
END; $$
    LANGUAGE PLPGSQL;
