create table images (
    id uuid not null,
    path varchar(255),
    hash bigint,
    primary key (id)
);
