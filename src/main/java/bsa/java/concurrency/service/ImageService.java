package bsa.java.concurrency.service;

import bsa.java.concurrency.exception.ImageNotFoundException;
import bsa.java.concurrency.exception.InvalidParameterException;
import bsa.java.concurrency.fs.FileSystem;
import bsa.java.concurrency.image.ImageHash;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.entity.Image;
import bsa.java.concurrency.repository.ImageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

@Service
public class ImageService {
    public static final Logger logger = LoggerFactory.getLogger(ImageService.class);

    @Autowired
    AsyncImageHandler asyncImageHandler;

    @Autowired
    ImageRepository imageRepository;

    @Autowired
    FileSystem filesystem;

    @Value("${upload.path}")
    private String uploadPath;

    public void uploadImages(MultipartFile[] files) {
        logger.info("Upload" + files.length + "images");

        CompletableFuture.allOf(Stream.of(files)
                .map(file -> asyncImageHandler.asyncProcessImage(file, null))
                .toArray(CompletableFuture[]::new))
                .join();
    }

    public List<SearchResultDTO> search(MultipartFile file, double threshold) {
        logger.info("Search images");

        if (threshold <= 0 || threshold > 1) {
            throw new InvalidParameterException("Threshold should be (0, 1]");
        }

        Long imageHash;
        try {
            imageHash = ImageHash.calcHash(file.getBytes());
        } catch (IOException e) {
            throw new InvalidParameterException("Unable to process multipartfile " + file.getName());
        }

        List<SearchResultDTO> list = imageRepository.findByHashHamming(imageHash, threshold);
        if (list.isEmpty()) {
            logger.info("No images found. Adding image");
            asyncImageHandler.asyncProcessImage(file, imageHash);
        }
        return list;
    }

    public void delete(UUID imageId) {
        logger.info("Delete image. uuid = " + imageId);

        Optional<Image> image = imageRepository.findById(imageId);
        if (image.isPresent()) {
            filesystem.deleteFile(image.get().getPath());
            imageRepository.deleteById(imageId);
        } else {
            throw new ImageNotFoundException(imageId);
        }

    }

    public void deleteAll() {
        logger.info("Delete all images");

        filesystem.deleteAll(uploadPath);
        imageRepository.deleteAll();
    }
}
