package bsa.java.concurrency.service;

import bsa.java.concurrency.exception.InvalidParameterException;
import bsa.java.concurrency.fs.FileSystem;
import bsa.java.concurrency.image.ImageHash;
import bsa.java.concurrency.image.entity.Image;
import bsa.java.concurrency.repository.ImageRepository;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Component
public class AsyncImageHandler {

    @Autowired
    FileSystem fileSystem;

    @Autowired
    ImageRepository imageRepository;

    @Value("${upload.path}")
    private String uploadPath;

    @Value("${upload.handlerpath}")
    private String handlerPath;

    @Async
    public CompletableFuture<Boolean> asyncProcessImage(MultipartFile multipartFile, Long imageHash) {
        byte[] byteArray = new byte[0];
        try {
            byteArray = multipartFile.getBytes();
        } catch (IOException e) {
            throw new InvalidParameterException("Unable to process multipartfile " + multipartFile.getName());
        }

        UUID imageUUID = UUID.randomUUID();
        String extension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());

        CompletableFuture<String> completableFuture =
                fileSystem.saveFile(uploadPath + "/" + imageUUID.toString() + "." + extension, byteArray);

        //create entity
        Image imageEntity = new Image();
        imageEntity.setId(imageUUID);
        imageEntity.setHash(imageHash == null ? ImageHash.calcHash(byteArray) : imageHash);
        try {
            imageEntity.setPath(handlerPath + "/" + completableFuture.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return CompletableFuture.completedFuture(false);
        }

        //persist
        imageRepository.save(imageEntity);

        return CompletableFuture.completedFuture(true);
    }

}
