package bsa.java.concurrency.exception;

import java.util.UUID;

public class ImageNotFoundException extends RuntimeException {
    public ImageNotFoundException(UUID id) {
        super("Image with id not found, id = " + id.toString());
    }
}
