package bsa.java.concurrency.repository;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.entity.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface ImageRepository extends JpaRepository<Image, UUID> {

    @Query(value = "SELECT " +
            "CAST(id as varchar) AS \"ImageId\", " +
            "cmp(hash, ?1) AS \"MatchPercent\", " +
            "path AS \"ImageUrl\" " +
            "FROM images " +
            "WHERE cmp(hash, ?1) >= ?2",
            nativeQuery = true)
    List<SearchResultDTO> findByHashHamming(Long curHash, double threshold);
}
