package bsa.java.concurrency.fs;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;

@Component
public class FileSystemImpl implements FileSystem {

    @Override
    public CompletableFuture<String> saveFile(String path, byte[] file) {
        try {
            File filePath = new File(path);
            FileUtils.writeByteArrayToFile(filePath, file);
            return CompletableFuture.completedFuture(filePath.getName());
        } catch (IOException e) {
            e.printStackTrace();
            return CompletableFuture.completedFuture("");
        }
    }

    @Override
    public CompletableFuture<Boolean> deleteFile(String path) {
        Boolean result = FileUtils.deleteQuietly(new File(path));
        return CompletableFuture.completedFuture(result);
    }

    @Override
    public CompletableFuture<Boolean> deleteAll(String path) {
        try {
            File file = new File(path);
            FileUtils.deleteDirectory(file);
            file.mkdirs();
            return CompletableFuture.completedFuture(true);
        } catch (IOException e) {
            e.printStackTrace();
            return CompletableFuture.completedFuture(false);
        }
    }

}
