package bsa.java.concurrency.fs;

import org.springframework.scheduling.annotation.Async;

import java.util.concurrent.CompletableFuture;

public interface FileSystem {
    @Async
    CompletableFuture<String> saveFile(String path, byte[] file);

    @Async
    CompletableFuture<Boolean> deleteFile(String path);

    @Async
    CompletableFuture<Boolean> deleteAll(String path);
}
