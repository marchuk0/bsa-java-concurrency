package bsa.java.concurrency.image;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

public class ImageHash {

    private static final int WIDTH = 9;
    private static final int HEIGHT = 9;

    public static long calcHash(byte[] array) {
        BufferedImage img = resizeAndGrayImage(readImage(array));
        long hash = 0;

        for (int x = 1; x < WIDTH; x++)
            for (int y = 1; y < HEIGHT; y++) {
                int prev = brightnessScore(img.getRGB(x - 1, y - 1));
                int cur = brightnessScore(img.getRGB(x, y));
                hash |= cur > prev ? 1 : 0;
                hash <<= 1;
            }

        return hash;
    }

    private static int brightnessScore(int rgb) {
        return rgb & 0b11111111;
    }

    private static BufferedImage resizeAndGrayImage(BufferedImage image) {
        Image tmp = image.getScaledInstance(WIDTH, HEIGHT, Image.SCALE_SMOOTH);
        BufferedImage dimg = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_BYTE_GRAY);

        Graphics2D g2d = dimg.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();

        return dimg;
    }

    private static BufferedImage readImage(byte[] array) {
        try (var stream = new ByteArrayInputStream(array)) {
            return ImageIO.read(stream);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

}
